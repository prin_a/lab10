package ku.util;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
/**
 * Stack test case.
 * @author Prin Angkunanuwat
 * 
 */
public class StackTest {

	private Stack stack;
	private static int capacity = 3;
	/** "Before" method is run before each test. */
	@Before
	public void setUp( ) {
		// set stack type 0 or 1
		StackFactory.setStackType(1);
		stack = StackFactory.makeStack(capacity);
	}
	/**
	 * New stack should empty.
	 */
	@Test
	public void newStackIsEmpty() {
		assertTrue( stack.isEmpty() );
		assertFalse( stack.isFull() );
		assertEquals( 0, stack.size() );		
	}
	/** pop() should throw an exception if stack is empty */
	@Test( expected=java.util.EmptyStackException.class )
	public void testPopEmptyStack() {
		Assume.assumeTrue( stack.isEmpty() );
		stack.pop();
		// this is unnecessary. For documentation only.
		fail("Pop empty stack should throw exception");
	}
	/**
	 * Pop() should remove and return the top elements.
	 */
	@Test
	public void testPopElement(){
		stack.push( 1 );
		stack.push( 2 );
		stack.push( 5 );
		assertEquals( 5, stack.pop() );
		assertEquals( 2, stack.pop() );
		assertEquals( 1, stack.pop() );
	}
	/**
	 * Peek() should not remove element.
	 */
	@Test
	public void testPeekElement(){
		stack.push( 1 );
		stack.push( 2 );
		assertEquals( 2, stack.peek() );
		assertEquals( 2, stack.peek() );
		assertSame( stack.peek() , stack.peek() );
		assertSame( stack.peek() , stack.pop() );
		assertEquals( 1 , stack.peek() );
		assertSame( stack.peek() , stack.pop() );
		assertEquals( null , stack.peek() );
		
	}
	
	/**
	 * If push the null object ,it should throw IllegalArgumentException.
	 */
	
	@Test( expected=java.lang.IllegalArgumentException.class )
	public void testPushNullObject(){
		stack.push(null);
		
	}
	/**
	 * If push when the stack is full ,it should throw IllegalStateException.
	 */
	@Test( expected=java.lang.IllegalStateException.class )
	public void testPushInFullStack(){
		stack.push( 1 );
		stack.push( 2 );
		stack.push( 3 );
		stack.push( 11 );
		
		
	}
	/**
	 * Should return true when stack is empty.
	 * False if stack is not empty.
	 */
	@Test
	public void testIsEmpty(){
		assertTrue( stack.isEmpty() );
		stack.push( 1 );
		assertFalse( stack.isEmpty() );
	}
	/**
	 * Should return true when stack is full.
	 * False if the stack is not full.
	 */
	@Test
	public void testIsFull(){
		assertFalse( stack.isFull() );
		stack.push( 1 );
		stack.push( 2 );
		assertFalse( stack.isFull() );
		stack.push( 3 );
		assertTrue( stack.isFull() );
	}	
	/**
	 * Capacity method should return the capacity of stack.
	 */
	@Test
	public void testCapacity(){
		assertEquals( 3, stack.capacity() );
		stack = StackFactory.makeStack(1);
		assertEquals( 1, stack.capacity() );
	}
	/**
	 * Testing size, size() should return the number of item in stack.
	 * Size does not change when peek and the stack is full.
	 */
	@Test
	public void testSize(){
		assertEquals( 0, stack.size() );
		stack.push( 1 );
		assertEquals( 1, stack.size() );
		stack.push( 2 );
		assertEquals( 2, stack.size() );
		stack.push( 3 );
		assertEquals( 3, stack.size() );
		stack.push( 4 );
		assertEquals( 3, stack.size() );
		stack.peek();
		assertEquals( 3, stack.size() );
		stack.pop();
		assertEquals( 2, stack.size() );
		
		
	}
	
	

}
